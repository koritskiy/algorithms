﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Algos.Models;

namespace Algos.Data
{
    public class AlgosContext : DbContext
    {
        public AlgosContext (DbContextOptions<AlgosContext> options)
            : base(options)
        {
        }

        public DbSet<Algos.Models.Algorithm> Algorithm { get; set; }
    }
}
