﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Algos.Data;
using Algos.Models;

namespace Algos.Pages.Algorithms
{
    public class CreateModel : PageModel
    {
        private readonly Algos.Data.AlgosContext _context;

        public CreateModel(Algos.Data.AlgosContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        public string Code { get; set; }
        public string Error { get; set; }

        [BindProperty]
        public Algorithm Algorithm { get; set; }

 
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Algorithm.Add(Algorithm);
            await _context.SaveChangesAsync();

            Code = Algorithm.Code;

            return RedirectToPage("./Index");
        }
    }
}
