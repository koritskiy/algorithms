﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Algos.Data;
using Algos.Models;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System.Reflection;
using Microsoft.CodeAnalysis.Text;
using System.Text;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.IO;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

namespace Algos.Pages.Algorithms
{
    public class DetailsModel : PageModel
    {
        private readonly Algos.Data.AlgosContext _context;

        public DetailsModel(Algos.Data.AlgosContext context)
        {
            _context = context;
        }

        public Algorithm Algorithm { get; set; }
        public object ResultInfo { get; set; }
        public List<List<int>> Values { get; set; }

        public static List<List<int>> GetListValuesFromString(string source)
        {
            string res = String.Empty;

            int lengthArray = int.Parse(source[0].ToString());
            int lengthSection = lengthArray + (lengthArray - 1) * 2 + 2 + 1;

            for (int i = 2; i < source.Length; i++)
            {
                res += source[i];
            }

            List<List<int>> status = new List<List<int>>();
            int count = 0;
            for (int i = 0; i < res.Length; i += lengthSection)
            {
                List<int> newList = new List<int>();
                for (int j = i; j < lengthSection + i - 1; j++)
                {
                    if (Char.IsDigit(res[j]))
                    {
                        newList.Add(int.Parse(res[j].ToString()));
                    }
                }
                status.Add(newList);
                count++;
            }

            return status;
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Algorithm = await _context.Algorithm.FirstOrDefaultAsync(m => m.ID == id);

            if (Algorithm == null)
            {
                return NotFound();
            }

            string programText = Algorithm.Code;

            string message = String.Empty;
            string infoMain = String.Empty;

            // Анализ всего код в принципе
            SyntaxTree tree = CSharpSyntaxTree.ParseText(programText);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            message = $"The tree is a {root.Kind()} node. The tree has {root.Members.Count} elements in it." +
                $"The tree has {root.Usings.Count} using statements." + Environment.NewLine;

            // Все, что входит в пространство имен namespace ExampleText
            MemberDeclarationSyntax firstMember = root.Members[0];
            message += ($"The first member is a {firstMember.Kind()}.");
            var helloWorldDeclaration = (NamespaceDeclarationSyntax)firstMember;

            message += ($"There are {helloWorldDeclaration.Members.Count} members declared in this namespace.");
            message += ($"The first member is a {helloWorldDeclaration.Members[0].Kind()}.");

            // Узнали, что первый и единственный элемент в namespace - класс (Program). Теперь надо копать еще глубже. 
            // Выведет, что внутри этого класса один только один метод - Main.  
            var programDeclaration = (ClassDeclarationSyntax)helloWorldDeclaration.Members[0];
            message += ($"There are {programDeclaration.Members.Count} members declared in the {programDeclaration.Identifier} class.");
            message += ($"The first member is a {programDeclaration.Members[0].Kind()}.");
            var mainDeclaration = (MethodDeclarationSyntax)programDeclaration.Members[0];

            // Теперь посмотрим, что там в Main
            infoMain = ($"The return type of the {mainDeclaration.Identifier} method is {mainDeclaration.ReturnType}.");
            infoMain += ($"The method has {mainDeclaration.ParameterList.Parameters.Count} parameters.");
            infoMain += ($"The body text of the {mainDeclaration.Identifier} method follows:");
            infoMain += (mainDeclaration.Body.ToFullString());

            var codeToEval = mainDeclaration.Body.ToFullString();

            try
            {
                var result = await CSharpScript.EvaluateAsync(codeToEval, ScriptOptions.Default.WithImports("System"));
                using (StringWriter stringWriter = new StringWriter())
                {
                    Console.SetOut(stringWriter);

                    //All console outputs goes here
                    Console.WriteLine(result);

                    string consoleOutput = stringWriter.ToString();
                    string path = @"Pages\Algorithms\OutputFiles\" + Algorithm.ID.ToString() + ".txt";
                    System.IO.File.WriteAllText(path, consoleOutput);
                }
                ResultInfo = result;
            } catch(CompilationErrorException e)
            {
                ResultInfo = e.Message;
            }
            catch (Exception e)
            {
                ResultInfo = e.Message;
            }

            Values = GetListValuesFromString((string)ResultInfo);

            Values = Values.Distinct().ToList();

            return Page();
        }
    }
}
