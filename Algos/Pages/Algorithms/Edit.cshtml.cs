﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Algos.Data;
using Algos.Models;

namespace Algos.Pages.Algorithms
{
    public class EditModel : PageModel
    {
        private readonly Algos.Data.AlgosContext _context;

        public EditModel(Algos.Data.AlgosContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Algorithm Algorithm { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Algorithm = await _context.Algorithm.FirstOrDefaultAsync(m => m.ID == id);

            if (Algorithm == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Algorithm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                string path = @"Pages\Algorithms\AlgorithmFiles\" + Algorithm.ID.ToString() + ".txt";
                System.IO.File.WriteAllText(path, Algorithm.Code);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlgorithmExists(Algorithm.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception)
            {

            }

            return RedirectToPage("./Index");
        }

        private bool AlgorithmExists(int id)
        {
            return _context.Algorithm.Any(e => e.ID == id);
        }
    }
}
