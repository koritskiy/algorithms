﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Algos.Data;
using Algos.Models;

namespace Algos.Pages.Algorithms
{
    public class IndexModel : PageModel
    {
        private readonly Algos.Data.AlgosContext _context;

        public IndexModel(Algos.Data.AlgosContext context)
        {
            _context = context;
        }

        public IList<Algorithm> Algorithm { get;set; }

        public async Task OnGetAsync()
        {
            Algorithm = await _context.Algorithm.ToListAsync();
        }
    }
}
